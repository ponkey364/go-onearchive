package vcard

import (
	"gitlab.com/ponkey364/go-onearchive/utils"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

type VCard struct {
	FullName     string
	Nickname     string
	Emails       []string
	PhoneNumbers []string
}

func ParseVCard(location string) ([]*VCard, error) {
	file, err := os.Open(location)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	dataString := string(data)

	set := regexp.MustCompile("((\r\n)|\n)").Split(dataString, -1)
	modSet := set // Duplicate the set, we'll modify this one

	var cards []*VCard

	for {
		start := utils.SliceIndex(modSet, "BEGIN:VCARD")
		end := utils.SliceIndex(modSet, "END:VCARD")

		if start == -1 {
			break
		}
		thisArea := modSet[start+1 : end]

		var (
			fn           string
			nick         string
			emails       []string
			phoneNumbers []string
		)

		for _, item := range thisArea {

			split := strings.Split(item, ":")

			switch split[0] {
			case "NICKNAME":
				nick = split[1]
			case "FN":
				fn = split[1]
			}

			if strings.HasPrefix(split[0], "EMAIL") {
				emails = append(emails, strings.TrimSpace(split[1]))
			}

			if strings.HasPrefix(split[0], "TEL") {
				phoneNumbers = append(phoneNumbers, strings.TrimSpace(split[1]))
			}
		}

		cards = append(cards, &VCard{
			FullName:     fn,
			Nickname:     nick,
			Emails:       emails,
			PhoneNumbers: phoneNumbers,
		})

		modSet = modSet[end+1:]
	}

	return cards, nil
}
