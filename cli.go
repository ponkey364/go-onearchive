package main

import (
	"gitlab.com/ponkey364/go-onearchive/parsers"
	"gitlab.com/ponkey364/go-onearchive/parsers/burxml"
	"gitlab.com/ponkey364/go-onearchive/parsers/fruitsql"
	"gitlab.com/ponkey364/go-onearchive/proto"
	"gitlab.com/ponkey364/go-onearchive/vcard"
	"gopkg.in/ukautz/clif.v1"
	"strconv"
	"strings"
)

var PlatformOpts = map[string]string{
	"1": "Google Hangouts / Chat (Directly from Google server) [NOT IMPLEMENTED]",
	"2": "Google Hangouts / Chat (JSON format, from hangouts_archiver) [NOT IMPLEMENTED]",
	"3": "iMessage (Incl. SMS)",
	"4": "SMS Backup & Restore",
	"5": "Signal Backup (Encryption Supported) [NOT IMPLEMENTED]",
}

func CLI_Main(cli clif.Input) {
	var (
		parser            parsers.Parser
		inputFileLocation string
		vCards            []*vcard.VCard
	)

	platform := cli.Choose("Which platform should I prepare to load data from?", PlatformOpts)
	if platform != "5" {
		inputFileLocation = cli.Ask("Where is the file for me to parse?", nil)
	}

	switch platform {
	case "1":
	case "2":
	case "5":
		{
			panic("not implemented")
		}
	case "3":
		{
			parser = &fruitsql.FruitParser{}
		}
	case "4":
		{
			parser = &burxml.XMLParser{}
		}
	}

	err := parser.Pull(inputFileLocation)
	if err != nil {
		panic(err)
	}

	chatMap, err := parser.ParseToChatMap()
	if err != nil {
		panic(err)
	}

	shouldLoadVCF := cli.Confirm("Should I load a vcf (contacts) file to put names to email addresses and phone numbers? [yes/no]")
	if shouldLoadVCF {
		VCFLocation := cli.Ask("Where is the file for me to parse?", nil)
		cards, err := vcard.ParseVCard(VCFLocation)
		if err != nil {
			panic(err)
		}
		vCards = cards
	} else {
		vCards = nil
	}

	wantsLibrary := cli.Confirm("Should I keep all separate chats in the same file? [yes/no]")

	if wantsLibrary {
		outputLocation := cli.Ask("Where should I output the file to?", nil)
		if !strings.HasSuffix(outputLocation, ".oa") {
			outputLocation = outputLocation + ".oa"
		}
		var roots []*proto.MsgRoot

		for _, chat := range chatMap {
			people, messages := BindPeopleInChat(chat, vCards)
			root := StapleToMessageRoot(people, messages)
			roots = append(roots, root)
		}

		main, err := MakeRoot(roots)
		if err != nil {
			panic(err)
		}

		err = WriteFile(main, outputLocation)
		if err != nil {
			panic(err)
		}
	} else {
		outputLocation := cli.Ask("Where should I output the files to [specify a folder]?", nil)

		index := 0
		for _, chat := range chatMap {
			people, messages := BindPeopleInChat(chat, vCards)
			root := StapleToMessageRoot(people, messages)
			main, err := MakeRoot(root)
			if err != nil {
				panic(err)
			}

			err = WriteFile(main, outputLocation+strconv.Itoa(index)+".oa")
			if err != nil {
				panic(err)
			}
		}
	}
}
