// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/onearchive.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Just in case, more than anything - it might be useful to track down dodgy converters / writers
type EncoderData struct {
	EncodedBy            string               `protobuf:"bytes,1,opt,name=encodedBy,proto3" json:"encodedBy,omitempty"`
	LastWritten          *timestamp.Timestamp `protobuf:"bytes,2,opt,name=lastWritten,proto3" json:"lastWritten,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *EncoderData) Reset()         { *m = EncoderData{} }
func (m *EncoderData) String() string { return proto.CompactTextString(m) }
func (*EncoderData) ProtoMessage()    {}
func (*EncoderData) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{0}
}

func (m *EncoderData) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EncoderData.Unmarshal(m, b)
}
func (m *EncoderData) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EncoderData.Marshal(b, m, deterministic)
}
func (m *EncoderData) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EncoderData.Merge(m, src)
}
func (m *EncoderData) XXX_Size() int {
	return xxx_messageInfo_EncoderData.Size(m)
}
func (m *EncoderData) XXX_DiscardUnknown() {
	xxx_messageInfo_EncoderData.DiscardUnknown(m)
}

var xxx_messageInfo_EncoderData proto.InternalMessageInfo

func (m *EncoderData) GetEncodedBy() string {
	if m != nil {
		return m.EncodedBy
	}
	return ""
}

func (m *EncoderData) GetLastWritten() *timestamp.Timestamp {
	if m != nil {
		return m.LastWritten
	}
	return nil
}

// The MainRoot is the top level for all of the data
// It's what gets written to disk
type MainRoot struct {
	Ed *EncoderData `protobuf:"bytes,1,opt,name=ed,proto3" json:"ed,omitempty"`
	// Types that are valid to be assigned to WhichRoot:
	//	*MainRoot_Sr
	//	*MainRoot_Lr
	WhichRoot            isMainRoot_WhichRoot `protobuf_oneof:"whichRoot"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *MainRoot) Reset()         { *m = MainRoot{} }
func (m *MainRoot) String() string { return proto.CompactTextString(m) }
func (*MainRoot) ProtoMessage()    {}
func (*MainRoot) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{1}
}

func (m *MainRoot) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MainRoot.Unmarshal(m, b)
}
func (m *MainRoot) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MainRoot.Marshal(b, m, deterministic)
}
func (m *MainRoot) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MainRoot.Merge(m, src)
}
func (m *MainRoot) XXX_Size() int {
	return xxx_messageInfo_MainRoot.Size(m)
}
func (m *MainRoot) XXX_DiscardUnknown() {
	xxx_messageInfo_MainRoot.DiscardUnknown(m)
}

var xxx_messageInfo_MainRoot proto.InternalMessageInfo

func (m *MainRoot) GetEd() *EncoderData {
	if m != nil {
		return m.Ed
	}
	return nil
}

type isMainRoot_WhichRoot interface {
	isMainRoot_WhichRoot()
}

type MainRoot_Sr struct {
	Sr *MsgRoot `protobuf:"bytes,2,opt,name=sr,proto3,oneof"`
}

type MainRoot_Lr struct {
	Lr *LibraryRoot `protobuf:"bytes,3,opt,name=lr,proto3,oneof"`
}

func (*MainRoot_Sr) isMainRoot_WhichRoot() {}

func (*MainRoot_Lr) isMainRoot_WhichRoot() {}

func (m *MainRoot) GetWhichRoot() isMainRoot_WhichRoot {
	if m != nil {
		return m.WhichRoot
	}
	return nil
}

func (m *MainRoot) GetSr() *MsgRoot {
	if x, ok := m.GetWhichRoot().(*MainRoot_Sr); ok {
		return x.Sr
	}
	return nil
}

func (m *MainRoot) GetLr() *LibraryRoot {
	if x, ok := m.GetWhichRoot().(*MainRoot_Lr); ok {
		return x.Lr
	}
	return nil
}

// XXX_OneofWrappers is for the internal use of the proto package.
func (*MainRoot) XXX_OneofWrappers() []interface{} {
	return []interface{}{
		(*MainRoot_Sr)(nil),
		(*MainRoot_Lr)(nil),
	}
}

type LibraryRoot struct {
	Roots                []*MsgRoot `protobuf:"bytes,1,rep,name=roots,proto3" json:"roots,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *LibraryRoot) Reset()         { *m = LibraryRoot{} }
func (m *LibraryRoot) String() string { return proto.CompactTextString(m) }
func (*LibraryRoot) ProtoMessage()    {}
func (*LibraryRoot) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{2}
}

func (m *LibraryRoot) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_LibraryRoot.Unmarshal(m, b)
}
func (m *LibraryRoot) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_LibraryRoot.Marshal(b, m, deterministic)
}
func (m *LibraryRoot) XXX_Merge(src proto.Message) {
	xxx_messageInfo_LibraryRoot.Merge(m, src)
}
func (m *LibraryRoot) XXX_Size() int {
	return xxx_messageInfo_LibraryRoot.Size(m)
}
func (m *LibraryRoot) XXX_DiscardUnknown() {
	xxx_messageInfo_LibraryRoot.DiscardUnknown(m)
}

var xxx_messageInfo_LibraryRoot proto.InternalMessageInfo

func (m *LibraryRoot) GetRoots() []*MsgRoot {
	if m != nil {
		return m.Roots
	}
	return nil
}

type Msg struct {
	ID                   string               `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Text                 string               `protobuf:"bytes,2,opt,name=text,proto3" json:"text,omitempty"`
	IsFromMe             bool                 `protobuf:"varint,3,opt,name=isFromMe,proto3" json:"isFromMe,omitempty"`
	SenderId             uint32               `protobuf:"varint,4,opt,name=senderId,proto3" json:"senderId,omitempty"`
	Date                 *timestamp.Timestamp `protobuf:"bytes,5,opt,name=date,proto3" json:"date,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Msg) Reset()         { *m = Msg{} }
func (m *Msg) String() string { return proto.CompactTextString(m) }
func (*Msg) ProtoMessage()    {}
func (*Msg) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{3}
}

func (m *Msg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Msg.Unmarshal(m, b)
}
func (m *Msg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Msg.Marshal(b, m, deterministic)
}
func (m *Msg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Msg.Merge(m, src)
}
func (m *Msg) XXX_Size() int {
	return xxx_messageInfo_Msg.Size(m)
}
func (m *Msg) XXX_DiscardUnknown() {
	xxx_messageInfo_Msg.DiscardUnknown(m)
}

var xxx_messageInfo_Msg proto.InternalMessageInfo

func (m *Msg) GetID() string {
	if m != nil {
		return m.ID
	}
	return ""
}

func (m *Msg) GetText() string {
	if m != nil {
		return m.Text
	}
	return ""
}

func (m *Msg) GetIsFromMe() bool {
	if m != nil {
		return m.IsFromMe
	}
	return false
}

func (m *Msg) GetSenderId() uint32 {
	if m != nil {
		return m.SenderId
	}
	return 0
}

func (m *Msg) GetDate() *timestamp.Timestamp {
	if m != nil {
		return m.Date
	}
	return nil
}

type Person struct {
	// More to come
	SenderID             uint32   `protobuf:"varint,1,opt,name=senderID,proto3" json:"senderID,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Person) Reset()         { *m = Person{} }
func (m *Person) String() string { return proto.CompactTextString(m) }
func (*Person) ProtoMessage()    {}
func (*Person) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{4}
}

func (m *Person) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Person.Unmarshal(m, b)
}
func (m *Person) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Person.Marshal(b, m, deterministic)
}
func (m *Person) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Person.Merge(m, src)
}
func (m *Person) XXX_Size() int {
	return xxx_messageInfo_Person.Size(m)
}
func (m *Person) XXX_DiscardUnknown() {
	xxx_messageInfo_Person.DiscardUnknown(m)
}

var xxx_messageInfo_Person proto.InternalMessageInfo

func (m *Person) GetSenderID() uint32 {
	if m != nil {
		return m.SenderID
	}
	return 0
}

func (m *Person) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type MsgRoot struct {
	PeopleInChat         []*Person `protobuf:"bytes,1,rep,name=peopleInChat,proto3" json:"peopleInChat,omitempty"`
	Messages             []*Msg    `protobuf:"bytes,2,rep,name=messages,proto3" json:"messages,omitempty"`
	Name                 string    `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *MsgRoot) Reset()         { *m = MsgRoot{} }
func (m *MsgRoot) String() string { return proto.CompactTextString(m) }
func (*MsgRoot) ProtoMessage()    {}
func (*MsgRoot) Descriptor() ([]byte, []int) {
	return fileDescriptor_83202f863d0d2602, []int{5}
}

func (m *MsgRoot) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MsgRoot.Unmarshal(m, b)
}
func (m *MsgRoot) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MsgRoot.Marshal(b, m, deterministic)
}
func (m *MsgRoot) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MsgRoot.Merge(m, src)
}
func (m *MsgRoot) XXX_Size() int {
	return xxx_messageInfo_MsgRoot.Size(m)
}
func (m *MsgRoot) XXX_DiscardUnknown() {
	xxx_messageInfo_MsgRoot.DiscardUnknown(m)
}

var xxx_messageInfo_MsgRoot proto.InternalMessageInfo

func (m *MsgRoot) GetPeopleInChat() []*Person {
	if m != nil {
		return m.PeopleInChat
	}
	return nil
}

func (m *MsgRoot) GetMessages() []*Msg {
	if m != nil {
		return m.Messages
	}
	return nil
}

func (m *MsgRoot) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func init() {
	proto.RegisterType((*EncoderData)(nil), "proto.EncoderData")
	proto.RegisterType((*MainRoot)(nil), "proto.MainRoot")
	proto.RegisterType((*LibraryRoot)(nil), "proto.LibraryRoot")
	proto.RegisterType((*Msg)(nil), "proto.Msg")
	proto.RegisterType((*Person)(nil), "proto.Person")
	proto.RegisterType((*MsgRoot)(nil), "proto.MsgRoot")
}

func init() { proto.RegisterFile("proto/onearchive.proto", fileDescriptor_83202f863d0d2602) }

var fileDescriptor_83202f863d0d2602 = []byte{
	// 395 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x90, 0x4f, 0x8b, 0xd4, 0x40,
	0x10, 0xc5, 0x4d, 0x67, 0x66, 0x4d, 0x2a, 0xee, 0x1e, 0xfa, 0x20, 0x61, 0x10, 0x0c, 0x61, 0x91,
	0x39, 0x65, 0x70, 0xf7, 0xe2, 0xc1, 0xd3, 0x3a, 0x8a, 0x01, 0x03, 0xd2, 0x08, 0x9e, 0x7b, 0x26,
	0x65, 0xd2, 0x90, 0x74, 0x87, 0xee, 0x56, 0x77, 0xce, 0x7e, 0x02, 0xbf, 0xb1, 0xa4, 0xf3, 0x67,
	0x32, 0x78, 0xd8, 0x53, 0xba, 0xaa, 0x7e, 0x55, 0xef, 0xe5, 0xc1, 0xcb, 0x4e, 0x2b, 0xab, 0x76,
	0x4a, 0x22, 0xd7, 0xc7, 0x5a, 0xfc, 0xc2, 0xcc, 0x35, 0xe8, 0xda, 0x7d, 0x36, 0xaf, 0x2b, 0xa5,
	0xaa, 0x06, 0x77, 0xae, 0x3a, 0xfc, 0xfc, 0xb1, 0xb3, 0xa2, 0x45, 0x63, 0x79, 0xdb, 0x0d, 0x5c,
	0x2a, 0x20, 0xfa, 0x28, 0x8f, 0xaa, 0x44, 0xbd, 0xe7, 0x96, 0xd3, 0x57, 0x10, 0xa2, 0x2b, 0xcb,
	0x87, 0x53, 0xec, 0x25, 0xde, 0x36, 0x64, 0xe7, 0x06, 0x7d, 0x0f, 0x51, 0xc3, 0x8d, 0xfd, 0xae,
	0x85, 0xb5, 0x28, 0x63, 0x92, 0x78, 0xdb, 0xe8, 0x6e, 0x93, 0x0d, 0x1a, 0xd9, 0xa4, 0x91, 0x7d,
	0x9b, 0x34, 0xd8, 0x12, 0x4f, 0xff, 0x78, 0x10, 0x14, 0x5c, 0x48, 0xa6, 0x94, 0xa5, 0x29, 0x10,
	0x2c, 0x9d, 0x42, 0x74, 0x47, 0x87, 0xd5, 0x6c, 0x61, 0x84, 0x11, 0x2c, 0x69, 0x02, 0xc4, 0xe8,
	0x51, 0xe5, 0x66, 0x64, 0x0a, 0x53, 0xf5, 0xfb, 0x9f, 0x9f, 0x31, 0x62, 0x34, 0xbd, 0x05, 0xd2,
	0xe8, 0xd8, 0xbf, 0xb8, 0xf2, 0x45, 0x1c, 0x34, 0xd7, 0xa7, 0x89, 0x6a, 0xf4, 0x43, 0x04, 0xe1,
	0xef, 0x5a, 0x1c, 0xeb, 0xbe, 0x95, 0xde, 0x43, 0xb4, 0x20, 0xe8, 0x2d, 0xac, 0xb5, 0x52, 0xd6,
	0xc4, 0x5e, 0xe2, 0xff, 0x2f, 0xc3, 0x86, 0x61, 0xfa, 0xd7, 0x03, 0xbf, 0x30, 0x15, 0xbd, 0x01,
	0x92, 0xef, 0xc7, 0x5c, 0x48, 0xbe, 0xa7, 0x14, 0x56, 0x16, 0x1f, 0xad, 0xf3, 0x18, 0x32, 0xf7,
	0xa6, 0x1b, 0x08, 0x84, 0xf9, 0xa4, 0x55, 0x5b, 0xa0, 0x73, 0x16, 0xb0, 0xb9, 0xee, 0x67, 0x06,
	0x65, 0x89, 0x3a, 0x2f, 0xe3, 0x55, 0xe2, 0x6d, 0xaf, 0xd9, 0x5c, 0xd3, 0x0c, 0x56, 0x25, 0xb7,
	0x18, 0xaf, 0x9f, 0x4c, 0xd5, 0x71, 0xe9, 0x3b, 0xb8, 0xfa, 0x8a, 0xda, 0x28, 0xb9, 0xb8, 0x3a,
	0x78, 0x3b, 0x5f, 0x75, 0x0e, 0x25, 0x6f, 0x71, 0x72, 0xd8, 0xbf, 0xd3, 0x47, 0x78, 0x3e, 0xfe,
	0x1f, 0x7d, 0x0b, 0x2f, 0x3a, 0x54, 0x5d, 0x83, 0xb9, 0xfc, 0x50, 0x73, 0x3b, 0xa6, 0x70, 0x3d,
	0xa6, 0x30, 0xdc, 0x67, 0x17, 0x08, 0x7d, 0x03, 0x41, 0x8b, 0xc6, 0xf0, 0x0a, 0x4d, 0x4c, 0x1c,
	0x0e, 0x8b, 0xd0, 0xe6, 0xd9, 0xac, 0xec, 0x9f, 0x95, 0x0f, 0x57, 0x0e, 0xbc, 0xff, 0x17, 0x00,
	0x00, 0xff, 0xff, 0x86, 0x62, 0xe8, 0x88, 0xb6, 0x02, 0x00, 0x00,
}
