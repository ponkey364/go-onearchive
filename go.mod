module gitlab.com/ponkey364/go-onearchive

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/golang/protobuf v1.4.0
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/olekukonko/ts v0.0.0-20171002115256-78ecb04241c0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/ukautz/reflekt v0.0.0-20180611090553-6ce38d64d188 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	gopkg.in/ukautz/clif.v1 v1.0.0-20190218144324-df36acc24204
)
