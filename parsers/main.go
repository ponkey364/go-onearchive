package parsers

import (
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/ponkey364/go-onearchive/proto"
)

// Very similar to proto.Msg, apart from uint32 SenderID -> string sender because we'll do the linking later
type ProxyMessage struct {
	ID       string
	Text     string
	IsFromMe bool
	Sender   string
	Date     *timestamp.Timestamp
}

type PChatMap map[string][]*ProxyMessage
type ChatMap map[string][]*proto.Msg

type Parser interface {
	Pull(string) error
	// The chat map is just a the below type - the key string is the ID of the chat
	// Also, we treat it as if it is a library, even if it isn't. We can split into separate file if necessary
	ParseToChatMap() (PChatMap, error)
}
