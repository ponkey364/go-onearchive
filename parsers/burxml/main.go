package burxml

import (
	"encoding/xml"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/ponkey364/go-onearchive/parsers"
	"io/ioutil"
	"os"
	"strconv"
	"time"
)

type XMLParser struct {
	parsers.Parser
	rows []*SMS
}

type SMSes struct {
	XMLName    xml.Name `xml:"smses"`
	Count      int      `xml:"count,attr"`
	BackupDate string   `xml:"backup_date,attr"`
	Messages   []*SMS   `xml:"sms"`
}

// Half these fields are very useless and or optional
type SMS struct {
	XMLName  xml.Name `xml:"sms"`
	Protocol int      `xml:"protocol,attr"`
	Address  string   `xml:"address,attr"`
	Date     int      `xml:"date,attr"` // In weird time (java)

	Type    string `xml:"type,attr"`
	Subject string `xml:"subject,attr"`
	Body    string `xml:"body,attr"`
	//ServiceCentre string   `xml:"service_centre,attr"`
	//Read          bool     `xml:"read,attr"`
	//Locked        bool     `xml:"locked,attr"`
	//DateSent      string   `xml:"date_sent,attr"`

	// I'm told this is optional, again, not in my testing file lol.
	ReadableDate *string `xml:"readable_date,attr"`
	// So **IN THEORY* this is a string, it might however, contain a phone number, it might be null
	// I don't know, the file I'm testing with only has contacts in.
	// Looking at their website, this is optional, so hey ho
	ContactName *string `xml:"contact_name,attr"`
}

func (xp *XMLParser) Pull(path string) error {
	xmlFile, err := os.Open(path)
	if err != nil {
		return err
	}

	xmlBytes, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		return err
	}

	var smses SMSes

	err = xml.Unmarshal(xmlBytes, &smses)
	if err != nil {
		return err
	}

	xp.rows = smses.Messages

	return nil
}

func (xp *XMLParser) ParseToChatMap() (parsers.PChatMap, error) {
	chatMap := make(parsers.PChatMap)

	for i, message := range xp.rows {
		var date time.Time

		if message.ReadableDate != nil {
			var err error

			date, err = time.Parse("2 Jan 2006 15:04:05", *message.ReadableDate)
			if err != nil {
				return nil, err
			}
		} else {
			// can i have an F in the chat for my brain kthx
			date = time.Unix(int64(message.Date/1000), 0)
		}

		var (
			isFromMe bool
			sender   string
		)

		switch message.Type {
		case "1":
			isFromMe = false
			sender = message.Address
		case "2":
			isFromMe = true
			sender = "me"
		}

		chatMap[message.Address] = append(chatMap[message.Address], &parsers.ProxyMessage{
			ID:       strconv.Itoa(i),
			Text:     message.Body,
			IsFromMe: isFromMe,
			Sender:   sender,
			Date:     &timestamp.Timestamp{Seconds: date.Unix()},
		})
	}

	return chatMap, nil
}
