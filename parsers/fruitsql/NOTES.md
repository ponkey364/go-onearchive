i'm trying to figure out what's going on here because Apple never cease to amaze with iMessage

We need to start at `chat`, then link on `chat_message` and then to `messages`, **but then**, to make this *more* fun, to find
out who send which messages (apart from making assumptions using `chat_handle_join` or `chat.guid`) we're going to have
to join [left] from messages to `handle` - **BUT, BUT**, sometimes: `message.handle` is 0 for reasons unknown to gnomekind (why null
isn't acceptable i'll never know) **SO**, if it's null, we need to fall back to `chat_handle_join` and figuring out
who sent which using `message.is_from_me` UGH