package fruitsql

import (
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/ponkey364/go-onearchive/parsers"
	"os"
	"time"
)

type FruitParser struct {
	parsers.Parser
	rows []*FruitRow
}

type FruitRow struct {
	ChatIdentifier string  `db:"chat_identifier"`
	GroupID        string  `db:"group_id"`
	Text           string  `db:"text"`
	HandleID       *string `db:"id"`
	IsFromMe       bool    `db:"is_from_me"`
	MessageGUID    string  `db:"msg_guid"`
	ChatGUID       string  `db:"chat_guid"`
	Date           string  `db:"date_"`
}

func (fp *FruitParser) cleanUp(path string) {
	os.Remove(path + "-wal")
	os.Remove(path + "-shm")
}

func (fp *FruitParser) Pull(path string) error {
	db, err := sqlx.Connect("sqlite3", path)
	if err != nil {
		return err
	}
	defer db.Close()
	defer fp.cleanUp(path)

	var rows []*FruitRow

	err = db.Select(&rows, `
		SELECT c.guid AS chat_guid, m.guid AS msg_guid, is_from_me, h.id, m.text, group_id, chat_identifier, datetime(m.date/1000000000 + strftime("%s", "2001-01-01"), "unixepoch","localtime" ) AS date_
		FROM chat_message_join
		JOIN chat c on chat_message_join.chat_id = c.ROWID
		JOIN message m on chat_message_join.message_id = m.ROWID
		LEFT JOIN handle h on m.handle_id = h.ROWID
		WHERE m.text is not null
	`)
	if err != nil {
		return err
	}

	fp.rows = rows

	return nil
}

func (fp *FruitParser) ParseToChatMap() (parsers.PChatMap, error) {
	chatMap := make(parsers.PChatMap)

	for _, message := range fp.rows {
		date, err := time.Parse("2006-01-02 15:04:05", message.Date)
		if err != nil {
			return nil, err
		}

		if _, ok := chatMap[message.ChatGUID]; !ok {
			chatMap[message.ChatGUID] = []*parsers.ProxyMessage{}
		}

		var sender string
		if message.HandleID == nil || message.IsFromMe {
			sender = "me"
		} else {
			sender = *message.HandleID
		}

		chatMap[message.ChatGUID] = append(chatMap[message.ChatGUID], &parsers.ProxyMessage{
			ID:       message.MessageGUID,
			Text:     message.Text,
			IsFromMe: message.IsFromMe,
			Sender:   sender,
			Date:     &timestamp.Timestamp{Seconds: date.Unix()},
		})
	}

	return chatMap, nil
}
