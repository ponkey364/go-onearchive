package main

import (
	"errors"
	"fmt"
	protobuf "github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/ponkey364/go-onearchive/parsers"
	"gitlab.com/ponkey364/go-onearchive/proto"
	"gitlab.com/ponkey364/go-onearchive/utils"
	"gitlab.com/ponkey364/go-onearchive/vcard"
	"gopkg.in/ukautz/clif.v1"
	"os"
	"strings"
	"time"
)

const ENCODED_BY = "OneArchive-RE-v1 go/1.13.5"

func BindPeopleInChat(messages []*parsers.ProxyMessage, vCards []*vcard.VCard) ([]*proto.Person, []*proto.Msg) {
	var (
		stampedMessages []*proto.Msg
		knownPeople     []*vcard.VCard
	)

	knownPeople = append(knownPeople, &vcard.VCard{FullName: "Me"}) // we need a nice way of handling 0: 'me'

msg:
	for _, message := range messages {
		if message.IsFromMe || message.Sender == "me" {
			stampedMessages = append(stampedMessages, &proto.Msg{
				ID:       message.ID,
				Text:     message.Text,
				IsFromMe: true,
				SenderId: 0,
				Date:     message.Date,
			})

			continue msg
		}

		// first, check any cards that we know of, hopefully, this'll speed things up dramatically
		for index, card := range knownPeople {
			if strings.Index(message.Sender, "@") != -1 {
				if utils.SliceIndex(card.Emails, message.Sender) != -1 {
					// we found an email
					stampedMessages = append(stampedMessages, &proto.Msg{
						ID:       message.ID,
						Text:     message.Text,
						IsFromMe: true,
						SenderId: uint32(index),
						Date:     message.Date,
					})
					continue msg
				}
			} else if strings.HasPrefix(message.Sender, "+") {
				if utils.SliceIndex(card.PhoneNumbers, message.Sender) != -1 {
					// we found an phone number
					stampedMessages = append(stampedMessages, &proto.Msg{
						ID:       message.ID,
						Text:     message.Text,
						IsFromMe: true,
						SenderId: uint32(index),
						Date:     message.Date,
					})
					continue msg
				}
			}
		}

		// now we can fall back to scanning the rest normally
		for _, card := range vCards {
			if strings.Index(message.Sender, "@") != -1 {
				// email test

				if utils.SliceIndex(card.Emails, message.Sender) != -1 {
					// found it
					stampedMessages = append(stampedMessages, &proto.Msg{
						ID:       message.ID,
						Text:     message.Text,
						IsFromMe: false,
						SenderId: uint32(len(knownPeople)), // we take this before appending so we don't have to subtract 1
						Date:     message.Date,
					})
					knownPeople = append(knownPeople, card)
					continue msg
				}
			} else if strings.HasPrefix(message.Sender, "+") {
				// The only thing i've seen output that we actually take into account for this is provided in a global
				// context with a phone number beginning with a + and international dialing code - again , this is liable to change
				if utils.SliceIndex(card.PhoneNumbers, message.Sender) != -1 {
					// found it
					stampedMessages = append(stampedMessages, &proto.Msg{
						ID:       message.ID,
						Text:     message.Text,
						IsFromMe: false,
						SenderId: uint32(len(knownPeople)), // we take this before appending so we don't have to subtract 1
						Date:     message.Date,
					})

					knownPeople = append(knownPeople, card)

					continue msg
				}
			}
		}
		// if we get down here, it's probs one of those short numbers or a service name like "twitter" or "authmsg"
		//spew.Dump("no dice", message.Sender)
		for index, known := range knownPeople {
			if message.Sender == known.FullName {
				stampedMessages = append(stampedMessages, &proto.Msg{
					ID:       message.ID,
					Text:     message.Text,
					IsFromMe: true,
					SenderId: uint32(index),
					Date:     message.Date,
				})
				continue msg
			}
		}

		stampedMessages = append(stampedMessages, &proto.Msg{
			ID:       message.ID,
			Text:     message.Text,
			IsFromMe: false,
			SenderId: uint32(len(knownPeople)), // we take this before appending so we don't have to subtract 1
			Date:     message.Date,
		})
		// we need to make a new known person with the name
		knownPeople = append(knownPeople, &vcard.VCard{
			FullName: message.Sender,
		})
	}

	var people []*proto.Person

	//spew.Dump(knownPeople)
	for index, person := range knownPeople {
		people = append(people, &proto.Person{
			SenderID: uint32(index),
			Name:     person.FullName,
		})
	}

	return people, stampedMessages
}

func StapleToMessageRoot(peopleInChat []*proto.Person, messages []*proto.Msg) *proto.MsgRoot {
	return &proto.MsgRoot{
		PeopleInChat: peopleInChat,
		Messages:     messages,
	}
}

func GetEncData() *proto.EncoderData {
	return &proto.EncoderData{
		EncodedBy: ENCODED_BY,
		LastWritten: &timestamp.Timestamp{
			Seconds: time.Now().Unix(),
		},
	}
}

func MakeRoot(whichRoot interface{}) (*proto.MainRoot, error) {
	switch root := whichRoot.(type) {
	case []*proto.MsgRoot:
		{
			return &proto.MainRoot{
				Ed: GetEncData(),
				WhichRoot: &proto.MainRoot_Lr{
					Lr: &proto.LibraryRoot{Roots: root},
				},
			}, nil
		}
	case *proto.MsgRoot:
		{
			return &proto.MainRoot{
				Ed:        GetEncData(),
				WhichRoot: &proto.MainRoot_Sr{Sr: root},
			}, nil
		}
	default:
		return nil, errors.New(fmt.Sprintf("bad type %T", root))
	}
}

func WriteFile(root *proto.MainRoot, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}

	wire, err := protobuf.Marshal(root)
	if err != nil {
		return err
	}

	_, err = file.Write(wire)
	if err != nil {
		return err
	}

	return file.Close()
}

func main() {
	cli := clif.New("OneArchive", "1.0.0", "OneArchive Utility")
	cli.New("interactive", "Use the interactive utility (recommended)", CLI_Main)

	cli.SetDefaultCommand("interactive")

	cli.Run()
}
